# Documentation
* [Auth API](#auth-api)

## Auth API
### POST /auth
Returns auth token.

|Param|Type|
|--|--|
|email|`string`|
|password|`string`|

Example request:
```json
{
    "email": "admin3@test.com",
    "password": "2222"
}
```
Example response:
```json
    "eyJhbGciOiJIUzI1Ni.6Y8vNf59spDgFkQB_K84FKnunuHWlG2pVVyr7W95-4I"
```
